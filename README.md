<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

# Déjà Dup Backups

Déjà Dup is a simple backup tool. It hides the complexity of backing up the
Right Way (encrypted, off-site, and regular) and uses
[duplicity](http://duplicity.nongnu.org/) as the backend.

 * Support for local, remote, or cloud backup locations such as Google Drive
 * Securely encrypts and compresses your data
 * Incrementally backs up, letting you restore from any particular backup
 * Schedules regular backups
 * Integrates well into your GNOME desktop

Déjà Dup focuses on ease of use and personal, accidental data loss.
If you need a full system backup or an archival program, you may prefer other
backup apps.

## Links

 * [Homepage](https://wiki.gnome.org/Apps/DejaDup)
 * [Download](https://wiki.gnome.org/Apps/DejaDup/Download)
 * [Get involved](https://wiki.gnome.org/Apps/DejaDup/GettingInvolved)
 * [Forums](https://discourse.gnome.org/tags/c/applications/7/deja-dup)

