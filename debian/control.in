Source: deja-dup
Section: utils
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: appstream-util,
               dbus,
               debhelper-compat (= 12),
               desktop-file-utils,
               dh-sequence-gnome,
               itstool,
               libhandy-1-dev,
               libglib2.0-bin,
               libglib2.0-dev (>= 2.64),
               libgpg-error-dev,
               libgtk-3-dev (>= 3.22),
               libjson-glib-dev (>= 1.2),
               libpackagekit-glib2-dev (>= 0.6.5),
               libsecret-1-dev (>= 0.18.6),
               libsoup2.4-dev (>= 2.48),
               meson (>= 0.47),
               pkg-config,
               valac (>= 0.36)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/gnome-team/deja-dup
Vcs-Git: https://salsa.debian.org/gnome-team/deja-dup.git
Homepage: https://launchpad.net/deja-dup

Package: deja-dup
Architecture: any
Depends: duplicity (>= 0.7.14), ${misc:Depends}, ${shlibs:Depends}
Recommends: gvfs-backends, packagekit, policykit-1
Description: Backup utility
 Déjà Dup is a simple backup tool. It hides the complexity of backing up the
 Right Way (encrypted, off-site, and regular) and uses duplicity as the
 backend.
 .
 Features:
  * Support for local, remote, or cloud backup locations such as Nextcloud
  * Securely encrypts and compresses your data
  * Incrementally backs up, letting you restore from any particular backup
  * Schedules regular backups
  * Integrates well into your GNOME desktop
